﻿using UnityEngine;
using System.Collections;

namespace CoAHomework
{
    public class ExerciseTwo : MonoBehaviour
    {
        public bool isGrounded;
        public bool canJump;

        public void Start()
        {
            
        }

        public void Update()
        {
            if (isGrounded == false)
            {
                canJump = true;
            }

            else
            {
                canJump = false;
            }

            Debug.Log(canJump);
        }
    }
}
