﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class ExerciseOne : MonoBehaviour
    {
        public bool isAlive;
        public int health = 100;

        public void Start()
        {

        }

        public void Update()
        {
            if (isAlive)
            {
                Debug.Log(health);

                if(health == 10)
                {
                    Debug.Log("You have 10 health left.");
                }

                if(health < 10)
                {
                    Debug.Log("You have less than 10 health. Be careful.");
                }
            }
        }
    }
}


