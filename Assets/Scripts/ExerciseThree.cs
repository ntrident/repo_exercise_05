﻿using UnityEngine;
using System.Collections;

namespace CoAHomework
{
    public class ExerciseThree : MonoBehaviour
    {
        public bool cheese;
        public bool ham;
        public bool egg;

        private void Update()
        {
            methodOne();
            methodTwo();
        }

        void methodOne()
        {
            if (cheese && ham && egg)
            {
                Debug.Log("We have everything for breakfast!");
            }


            else if ((cheese && ham) || (ham && egg) || (egg && cheese))
            {
                Debug.Log("We got almost everything!");
            }
        }

        void methodTwo()
        {
            if (ham || cheese || egg)
            {
                Debug.Log("We got something!");
            }
        }
    }
}
