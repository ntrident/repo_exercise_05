﻿using UnityEngine;
using System.Collections;

public enum PlayerState
{
    NONE,
    IDLE,
    WALKING,
    RUNNING,
}
