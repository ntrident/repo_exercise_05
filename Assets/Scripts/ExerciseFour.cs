﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CoAHomework
{
    public class ExerciseFour : MonoBehaviour
    {
        public PlayerState playerState;
        private float movementSpeed;


        void Update()
        {
            switch (playerState)
            {


                case PlayerState.RUNNING:
                    movementSpeed = 120f;
                    Debug.Log(movementSpeed);
                    Debug.Log(playerState);

                    break;


                case PlayerState.WALKING:
                    movementSpeed = 50f;
                    Debug.Log(movementSpeed);
                    Debug.Log(playerState);

                    break;


                case PlayerState.IDLE:
                    movementSpeed = 0f;
                    Debug.Log(movementSpeed);
                    Debug.Log(playerState);

                    break;


                case PlayerState.NONE:

                    movementSpeed = 0f;
                    Debug.Log(movementSpeed);
                    Debug.Log(playerState);

                    break;

            }          
        }
    }

}